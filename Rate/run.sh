#!/bin/bash


source ../global.sh
echo $analysis_dir
echo $stack_dir

rate_dir="${analysis_dir}Rate/"


cd $rate_dir

${stack_dir}DaVinci/run gaudirun.py rate_options.py

${stack_dir}DaVinci/run ${stack_dir}DaVinci/HltEfficiencyChecker/scripts/hlt_calculate_rates.py rate_ntuple.root --input-rate 1140 --json Hlt2_rates.json




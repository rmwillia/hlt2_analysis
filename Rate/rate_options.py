from Moore import options
from HltEfficiencyChecker.config import run_moore_with_tuples
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.hlt2_global_reco import ( reconstruction as hlt2_reconstruction, make_light_reco_pr_kf_without_UT, )
from RecoConf.ttrack_selections_reco import make_ttrack_reco
from RecoConf.global_tools import ( stateProvider_with_simplified_geom, trackMasterExtrapolator_with_simplified_geom, )
from RecoConf.calorimeter_reconstruction import make_digits
from RecoConf.hlt1_muonid import make_muon_hits

from Hlt2Conf.lines.rd.b_to_xll_hlt2 import all_lines , BuToKpEE_line
from Hlt2Conf.lines.rd.builders.b_to_xll_builders import make_rd_BToXll
from GaudiKernel.SystemOfUnits import MeV

def make_lines():
    lines = [builder() for builder in all_lines.values()]
    
    # For a builder with a decorator @configurable,
    # You can use .bind to overrule an argument
    # In this example I change the mass window (notice I need the units )
    with make_rd_BToXll.bind( am_min = 3_000. * MeV , am_max = 8_000. * MeV ):
        lines.append( BuToKpEE_line(name="Hlt2RD_BuToKpEE_B_builder_am_max_8000MeV___") )
    
    return lines
options.lines_maker = make_lines




options.set_input_and_conds_from_testfiledb("expected_2024_min_bias_hlt1_filtered")
options.evt_max = 1_000
options.input_raw_format = 0.5
options.print_freq = 100
options.ntuple_file = "rate_ntuple.root"


make_muon_hits.global_bind(geometry_version=3)
make_digits.global_bind(calo_raw_bank=True)


with (
    reconstruction.bind(from_file=False), 
    make_light_reco_pr_kf_without_UT.bind(skipRich=False, skipCalo=False, skipMuon=False), 
    make_ttrack_reco.bind(skipCalo=False), 
    hlt2_reconstruction.bind(make_reconstruction=make_light_reco_pr_kf_without_UT),
):
    config = run_moore_with_tuples(options, False, public_tools = [ trackMasterExtrapolator_with_simplified_geom(), stateProvider_with_simplified_geom(), ] )













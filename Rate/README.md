# Rate code

## What you need to change
To change the lines you want to run over, in ```rate_options.py```, change lines 10 to 23.
For example to run over all ```b_to_v0ll_hlt2.py``` lines, change line 10:
```python
from Hlt2Conf.lines.rd.b_to_b0ll_hlt2 import all_lines
```

To run a line with a changed selection, use the ```.bind```.
For example for line ```b2kseell_line```, you may want to change the selection for ```make_rd_ks0_lls``` for one line.
To do this import the builder:
```python
from Hlt2Conf.lines.rd.b_to_b0ll_hlt2 import b2kseell_line
from Hlt2Conf.lines.rd.builders.rdbuilder_thor import make_rd_ks0_lls
from GaudiKernel.SystemOfUnits import GeV, MeV, ps
```
And then in the ```make_lines``` function add a context manager, to 
```python
with make_rd_ks0_lls.bind( bpvltime_min = 2. * ps ):
	lines.append( b2kseell_line(name = "Hlt2RD_BdToKSEE_LL__changedthing__") )
```


In rate options to change how many minbias events to run over, change line 17:
```python
options.evt_max = 1_000
```



## What you need to run
Run from the ```Rate``` directory, i.e., where this README.md lives, and run:
```bash
source run.sh
```






from Moore import options
from Hlt2Conf.lines.rd.b_to_xll_hlt2 import BuToKpEE_line

def make_lines():
        return [ BuToKpEE_line(name="Hlt2RD_BuToKpEE") ]
        
options.lines_maker = make_lines

from Moore import options, run_moore
from HltEfficiencyChecker.config import run_moore_with_tuples , run_chained_hlt_with_tuples
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction, make_fastest_reconstruction

decay = "${Bp}[B+ => ${Kp}K+ (J/psi(1S) => ${mup}mu+ ${mum}mu-) ]CC"

import sys
sys.path.insert(1,"/afs/cern.ch/work/r/rmwillia/hlt2/Efficiency/BuToKpMuMu")
from sim_list import files

sys.path.insert(1,"/afs/cern.ch/work/r/rmwillia/hlt2/Efficiency")
from config import evt_max , print_freq

options.input_files = []

for file in files:
    options.input_files.append(
      'root://eoslhcb.cern.ch/' + file
    )


options.input_type = 'ROOT'
options.input_raw_format = 0.5 
options.evt_max = evt_max
options.print_freq = print_freq

options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20231017'
options.conddb_tag = 'sim-20231017-vc-md100'
options.ntuple_file = "./eff_ntuple.root"






from RecoConf.calorimeter_reconstruction import make_digits
make_digits.global_bind(calo_raw_bank=False)

from RecoConf.hlt1_muonid import make_muon_hits
make_muon_hits.global_bind(geometry_version=3)

from RecoConf.decoders import default_ft_decoding_version
default_ft_decoding_version.global_bind(value=6)

from RecoConf.global_tools import stateProvider_with_simplified_geom
with reconstruction.bind(from_file=False), hlt2_reconstruction.bind(make_reconstruction = make_fastest_reconstruction):
        run_moore_with_tuples(options, False, decay, public_tools=[stateProvider_with_simplified_geom()])
        
        
        
        
        
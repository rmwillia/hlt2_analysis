from Moore import options
from Hlt2Conf.lines.rd.b_to_xll_hlt2 import BuToKpMuMu_line

def make_lines():
        return [ BuToKpMuMu_line(name="Hlt2RD_BuToKpMuMu") ]
        
options.lines_maker = make_lines

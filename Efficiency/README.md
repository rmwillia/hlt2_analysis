# Efficiency code


## First steps

Start off in the ```Efficiency``` directory.

### Make your directory
Once you know what line you want to run over, make a subdirectory from this one:
```b2kseell_line```
```bash
mkdir b2kseell_line
```

### lines.py
In here you will need a ```lines.py```, i.e.,
```python
from Moore import options
from Hlt2Conf.lines.rd.b_to_v0ll_hlt2 import b2kseell_line

def make_lines():
		lines = []
		lines.append( b2kseell_line(name="Hlt2RD_BdToKSEE_LL_nominal_") )
        return lines
        
options.lines_maker = make_lines
```

Note here you can do the same changes as in the rates README.md, i.e.,
```python
from Hlt2Conf.lines.rd.b_to_b0ll_hlt2 import b2kseell_line
from Hlt2Conf.lines.rd.builders.rdbuilder_thor import make_rd_ks0_lls
from GaudiKernel.SystemOfUnits import GeV, MeV, ps

def make_lines():
		lines = []
		lines.append( b2kseell_line(name="Hlt2RD_BdToKSEE_LL_nominal_") )
			
		with make_rd_ks0_lls.bind( bpvltime_min = 2. * ps ):
			lines.append( b2kseell_line(name = "Hlt2RD_BdToKSEE_LL__changedthing__") )
		
        return lines
```

### sim_list.py
Then you need a ```sim_list.py``` file. For this you need the eos directory for your simulation. For example, for BtoKEE:
```/eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00208986/0000/```
You then run ```source make_sim_list.sh BuToKpEE /eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00208986/0000/```

This will make a python file in the BuToKpEE subdirectory. I would then get your bkkpath and append the top line with ```# bkkpath```. This is purely for future reference.





### eff.py
Most complicated you need an options file



You will need to modify the decay descriptor to match your simulation,
```python
decay = "${Bp}[B+ => ${Kp}K+ (J/psi(1S) => ${ep}e+ ${em}e-) ]CC"
```
It's important you give names to the reconstructible children (not neutrals)




You will need to modify both the sys.path.insert lines.
i.e.,
```python
import sys
sys.path.insert(1,"/afs/cern.ch/work/r/rmwillia/hlt2_analysis/Efficiency/BuToKpEE")
from sim_list import files

sys.path.insert(1,"/afs/cern.ch/work/r/rmwillia/hlt2_analysis/Efficiency")
from config import evt_max , print_freq
```

You then need to modify the following
```python
options.input_type = 'ROOT'

options.input_raw_format = 0.5 

options.dddb_tag = 'dddb-20231017'

options.conddb_tag = 'sim-20231017-vc-md100'

make_digits.global_bind(calo_raw_bank=True)

make_muon_hits.global_bind(geometry_version=3)

default_ft_decoding_version.global_bind(value=6)
```

Guides for this are:
https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/different_samples.html
https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/running_over_mc.html



## modify run_eff.sh
You need to add to the ```run_eff.sh```:
```bash
elif [ $line == "BuToKpEE" ]; then    
    reconstructible_children=Kp,ep,em
    true_signal_to_match_to=Bp
```
Where the ```reconstructible_children``` and ```true_signal_to_match_to``` matches your descriptor in your ```eff.py```.


## run_eff.sh
To run, be in the ```Rate``` directory
```bash
source run_eff.sh BuToKpEE
```


#!/bin/bash



# Script run efficiency calculation
#
# positional args
# 1 -> line , refers to directory and options for reconstructible-children children
#
#


line=$1

echo "Line: ${line}"

source ../global.sh
echo $analysis_dir
echo $stack_dir

eff_dir="${analysis_dir}Efficiency/"
line_dir="${eff_dir}${line}/"




cd $line_dir
echo "Working directory: ${line_dir}"

rm --verbose ${line_dir}eff_ntuple.root
rm --verbose ${line_dir}EfficiencyResults.json





# run over MC
${stack_dir}DaVinci/run gaudirun.py ${line_dir}lines.py ${line_dir}eff.py






# define reco-children & truth matching
if [ $line == "BuToKpMuMu" ]; then
    reconstructible_children=Kp,mup,mum
    true_signal_to_match_to=Bp
	
elif [ $line == "BuToKpEE" ]; then    
    reconstructible_children=Kp,ep,em
    true_signal_to_match_to=Bp
	
fi

echo "Reconstructible Children: $reconstructible_children"
echo "Truth Matching: $true_signal_to_match_to"

${stack_dir}DaVinci/run \
	${stack_dir}DaVinci/HltEfficiencyChecker/scripts/hlt_line_efficiencies.py \
	${line_dir}eff_ntuple.root \
	--reconstructible-children=$reconstructible_children \
	--true-signal-to-match-to=$true_signal_to_match_to \
	--json="${line_dir}EfficiencyResults.json"

cd $eff_dir















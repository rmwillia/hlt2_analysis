#!/bin/bash



# Script to make python file of form
#
# list = [
# "..." ,
# .
# .
# .
# .
# ]
#
#
# positional args
# 1 -> line , refers to directory to put file into
# 2 -> directory, directory to get files, i.e., /eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00208986/0000/ for BuToKpEE
#
#
#



line=$1
dir=$2

cd $line

pyfile="./sim_list.py"

rm -i -v $pyfile




touch $pyfile

echo 'files = [' >> $pyfile

for f in $dir*
do
    echo "'$f'," >> $pyfile
done

echo ']' >> $pyfile


cd ../
